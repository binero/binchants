package eu.jeroenbollen.mcmods.binchants.bulkmine;

import java.util.ArrayDeque;
import java.util.Queue;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.OreBlock;
import net.minecraft.world.level.block.state.BlockState;

public class VeinCalculator extends QueueBasedCalculator {
    Queue<BlockPos> queue = new ArrayDeque<>();

    public VeinCalculator(final Level level, final BlockPos startingPos, final int maxVeinSize) {
        super(level, startingPos, maxVeinSize);
    }

    public static boolean isVeinable(final BlockState blockState) {
        return blockState.getBlock() instanceof OreBlock;
    }

    @Override
    protected boolean shouldVisit(final BlockState blockState) {
        return VeinCalculator.isVeinable(blockState);
    }

    @Override
    protected void expandQueueAround(final BlockPos pos) {
        this.expandQueueWith(pos.above());
        this.expandQueueWith(pos.below());
        this.expandQueueWith(pos.north());
        this.expandQueueWith(pos.east());
        this.expandQueueWith(pos.south());
        this.expandQueueWith(pos.west());
    }

    @Override
    protected boolean shouldBreak(BlockState blockState) {
        return true;
    }
}
