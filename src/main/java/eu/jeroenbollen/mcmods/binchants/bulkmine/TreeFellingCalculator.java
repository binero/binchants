package eu.jeroenbollen.mcmods.binchants.bulkmine;

import net.minecraft.core.BlockPos;
import net.minecraft.tags.BlockTags;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;

public class TreeFellingCalculator extends QueueBasedCalculator {
    final int enchantmentLevel;

    public TreeFellingCalculator(Level level, BlockPos startingPos, final int enchantmentLevel) {
        super(level, startingPos, TreeFellingCalculator.calculateMaxBulkSize(enchantmentLevel));
        this.enchantmentLevel = enchantmentLevel;

        if (this.enchantmentLevel >= 2) {
            this.expandQueueWith(startingPos.north());
            this.expandQueueWith(startingPos.east());
            this.expandQueueWith(startingPos.south());
            this.expandQueueWith(startingPos.west());
            this.expandQueueWith(startingPos.north().east());
            this.expandQueueWith(startingPos.south().east());
            this.expandQueueWith(startingPos.north().west());
            this.expandQueueWith(startingPos.south().west());
        }
    }

    public static boolean canFell(final BlockState blockState) {
        return blockState.is(BlockTags.LOGS);
    }

    @Override
    protected boolean shouldBreak(BlockState blockState) {
        return TreeFellingCalculator.canFell(blockState);
    }

    @Override
    protected boolean shouldVisit(BlockState blockState) {
        return blockState.is(BlockTags.LOGS) || blockState.is(BlockTags.LEAVES);
    }

    @Override
    protected void expandQueueAround(BlockPos pos) {
        final BlockPos above = pos.above();
        this.expandQueueWith(above);

        if (this.enchantmentLevel >= 3) {
            this.expandQueueWith(pos.north());
            this.expandQueueWith(pos.east());
            this.expandQueueWith(pos.south());
            this.expandQueueWith(pos.west());
            this.expandQueueWith(pos.north().east());
            this.expandQueueWith(pos.north().west());
            this.expandQueueWith(pos.south().east());
            this.expandQueueWith(pos.south().west());
            this.expandQueueWith(above.north());
            this.expandQueueWith(above.east());
            this.expandQueueWith(above.south());
            this.expandQueueWith(above.west());
            this.expandQueueWith(above.north().east());
            this.expandQueueWith(above.north().west());
            this.expandQueueWith(above.south().east());
            this.expandQueueWith(above.south().west());
        }
    }

    private static int calculateMaxBulkSize(final int enchantmentLevel) {
        switch (enchantmentLevel) {
            case 1:
                return 7;
            case 2:
                return 63;
            case 3:
                return 127;
            default:
                return 0;
        }
    }
}
