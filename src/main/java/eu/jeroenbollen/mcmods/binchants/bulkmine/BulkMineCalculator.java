package eu.jeroenbollen.mcmods.binchants.bulkmine;

import java.util.List;

import eu.jeroenbollen.mcmods.binchants.Binchants;
import net.minecraft.core.BlockPos;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;

public interface BulkMineCalculator {
    /**
     * Calculates a group of blocks that are mined up at once.
     * 
     * @return A list of blocks to mine up at once.
     */
    public List<BlockPos> calculateBulk();

    public default void breakBulk(final Level level, final Player player) {
        final List<BlockPos> bulk = this.calculateBulk();

        for (final BlockPos blockPos : bulk)
            Binchants.breakBlock(level, player, blockPos);

        final ItemStack toolStack = player.getMainHandItem();

        if (!level.isClientSide)
            toolStack.<LivingEntity>hurtAndBreak(bulk.size(), player,
                    e -> e.broadcastBreakEvent(EquipmentSlot.MAINHAND));
    }
}
