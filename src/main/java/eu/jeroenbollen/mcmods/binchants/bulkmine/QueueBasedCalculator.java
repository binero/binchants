package eu.jeroenbollen.mcmods.binchants.bulkmine;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Queue;
import java.util.Set;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;

public abstract class QueueBasedCalculator implements BulkMineCalculator {
    final int maxBulkSize;
    private final Level level;
    private Set<BlockPos> visitedPositions;
    private Queue<BlockPos> queue = new ArrayDeque<>();

    protected QueueBasedCalculator(final Level level, final BlockPos startingPos, final int maxBulkSize) {
        this.maxBulkSize = maxBulkSize;
        this.level = level;
        this.visitedPositions = new HashSet<>();
        this.visitedPositions.add(startingPos);
        this.expandQueueAround(startingPos);
    }

    public List<BlockPos> calculateBulk() {
        List<BlockPos> blocksToBreak = new ArrayList<>(this.maxBulkSize);

        while (!queue.isEmpty() && blocksToBreak.size() < this.maxBulkSize) {
            final BlockPos currentPos = this.queue.remove();
            if (this.shouldBreak(currentPos))
                blocksToBreak.add(currentPos);
            this.expandQueueAround(currentPos);
        }

        return blocksToBreak;
    }

    /**
     * Returns whether the visited block should actually be broken.
     * 
     * @param blockState The block that was visted.
     * @return True if the visited block should be broken.
     */
    protected abstract boolean shouldBreak(BlockState blockState);

    protected boolean shouldBreak(BlockPos blockPos) {
        return this.shouldBreak(this.level.getBlockState(blockPos));
    }

    protected abstract boolean shouldVisit(BlockState blockState);

    /**
     * Returns whether the given position is a valid block to add to the queue.
     * 
     * @param blockPos The position to test.
     * @return True when the position has a valid block to add to the queue.
     */
    protected boolean shouldVisit(BlockPos blockPos) {
        return this.shouldVisit(this.level.getBlockState(blockPos));
    };

    /**
     * Starting from the given position, expand the queue by one step.
     */
    protected abstract void expandQueueAround(final BlockPos pos);

    /**
     * Adds the given position to the queue, if it should be visited.
     * 
     * @param blockPos The position to add to the queue.
     */
    protected void expandQueueWith(final BlockPos blockPos) {
        if (this.visitedPositions.add(blockPos) && this.shouldVisit(blockPos))
            this.queue.add(blockPos);
    }
}
