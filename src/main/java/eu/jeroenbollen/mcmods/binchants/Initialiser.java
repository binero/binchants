package eu.jeroenbollen.mcmods.binchants;

import eu.jeroenbollen.mcmods.binchants.bulkmine.TreeFellingCalculator;
import eu.jeroenbollen.mcmods.binchants.bulkmine.VeinCalculator;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.event.player.PlayerBlockBreakEvents;
import net.minecraft.world.item.enchantment.EnchantmentHelper;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;
import net.minecraft.core.Registry;

public class Initialiser implements ModInitializer {
    @Override
    public void onInitialize() {
        Registry.register(Registry.ENCHANTMENT, new ResourceLocation("binchants", "felling"),
                Binchants.FELLER_ENCHANTMENT);
        Registry.register(Registry.ENCHANTMENT, new ResourceLocation("binchants", "vein"), Binchants.VEIN_ENCHANTMENT);

        PlayerBlockBreakEvents.AFTER.register((level, player, position, blockState, entity) -> {
            final ItemStack toolStack = player.getMainHandItem();
            final int veinLevel = EnchantmentHelper.getItemEnchantmentLevel(Binchants.VEIN_ENCHANTMENT, toolStack);

            if (veinLevel <= 0 || !VeinCalculator.isVeinable(blockState))
                return;

            final int maxVeinSize = Binchants.VEIN_ENCHANTMENT.getMaxVeinSize(veinLevel);

            final VeinCalculator veinCalculator = new VeinCalculator(level, position, maxVeinSize);
            veinCalculator.breakBulk(level, player);
        });

        PlayerBlockBreakEvents.AFTER.register((level, player, position, blockState, entity) -> {
            final ItemStack toolStack = player.getMainHandItem();
            final int veinLevel = EnchantmentHelper.getItemEnchantmentLevel(Binchants.FELLER_ENCHANTMENT, toolStack);

            if (veinLevel <= 0 || !TreeFellingCalculator.canFell(blockState))
                return;

            final TreeFellingCalculator treeFellingCalculator = new TreeFellingCalculator(level, position, veinLevel);
            treeFellingCalculator.breakBulk(level, player);
        });
    }
}
