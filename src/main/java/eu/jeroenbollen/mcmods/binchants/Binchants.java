package eu.jeroenbollen.mcmods.binchants;

import java.util.Collection;

import eu.jeroenbollen.mcmods.binchants.enchantments.FellerEnchantment;
import eu.jeroenbollen.mcmods.binchants.enchantments.VeinEnchantment;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.Level;

public abstract class Binchants {
    private Binchants() {
        throw new IllegalStateException("Cannot instantiate utility class Binchants");
    }

    public static final FellerEnchantment FELLER_ENCHANTMENT = new FellerEnchantment();
    public static final VeinEnchantment VEIN_ENCHANTMENT = new VeinEnchantment();

    public static void breakBlocks(final Level world, final Player player, final Collection<BlockPos> positions) {
        for (final BlockPos blockPos : positions)
            Binchants.breakBlock(world, player, blockPos);
    }

    public static void breakBlock(final Level level, final Player player, final BlockPos blockPos) {
        final BlockState blockState = level.getBlockState(blockPos);
        final Block block = blockState.getBlock();
        final BlockEntity blockEntity = level.getBlockEntity(blockPos);

        final boolean broken = level.removeBlock(blockPos, false);
        block.playerWillDestroy(level, blockPos, blockState, player);

        if (broken)
            block.destroy(level, blockPos, blockState);

        if (!player.isCreative() && broken) {
            final ItemStack toolStack = player.getMainHandItem();
            block.playerDestroy(level, player, blockPos, blockState, blockEntity, toolStack);
        }
    }
}
