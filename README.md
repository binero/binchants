# Binchants

Minecraft modification that adds some cool little enchantments to the game. They work even when only installed on the server, although they won't show up to the client. Clients need to install the modification too to be able to see the enchants.
